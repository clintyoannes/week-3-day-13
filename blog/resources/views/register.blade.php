<!DOCTYPE html>

<html>
    <head>
<title>Form Pendaftaran</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device=width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
    </head>
<body>
    <div>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        </div>

    <!--Form-->
    <form action = "/welcomeuser" method="post" >
        @csrf
        <label for="first_name" >First name</label><br>
        <input type="text" placeholder="First name" name ="nama1" id="first_name">
        <br><br>
        <label for="Last_name" >Last name</label><br>
        <input type="text" placeholder="Last name" name ="nama2" id="Last_name">
        <br><br>
        <Label>Gender:</Label> 
        <br>
        <input type="radio" name="gender" value="0"> Male<br>
        <input type="radio" name="gender" value="1"> Female <br>
        <input type="radio" name="gender" value="2"> Other
        <br><br>
        <label>Nationality:</label><br>
        <select>
            <option value ="Singapura">Singapura</option>
            <option value ="Indonesia">Indonesia</option>
            <option value ="Malaysia">Malaysia</option>
            <option value ="Thailand">Thailand</option>
        </select>
        <br><br>
        <Label>Language Spoken:</Label> <br>
        <input type="checkbox" name="Language" value="0"> Bahasa Indonesia<br>
        <input type="checkbox" name="Language" value="1"> English<br>
        <input type="checkbox" name="Language" value="2"> Other
        <br><br>
        <Label>Bio:</Label><br>
        <textarea cols="30" rows="10" id="pesan_user"></textarea>
            <br><br>
             <input type="submit" value="Kirim Data">
             
    </form>

</body>

</html>